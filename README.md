# Simple Prime Number Finder Backend

Backend that exposed API to returns to the user the highest prime number lower than the input number. For example an input of 55 would return 53.